﻿using ReflectionConversion.Policy;
using System;

namespace ReflectionConversion
{
    internal class Aggregate
    {
        public Guid Id { get; private set; }
        public IPolicy Policy { get; set; }

        internal Aggregate(Guid id, IPolicy policy)
        {
            Id = id;
            Policy = policy;
        }
    }
}