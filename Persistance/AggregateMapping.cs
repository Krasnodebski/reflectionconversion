﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ReflectionConversion.Policy;
using System;

namespace ReflectionConversion.Persistance
{
    internal class AggregateMapping : IEntityTypeConfiguration<Aggregate>
    {
        public void Configure(EntityTypeBuilder<Aggregate> builder)
        {
            builder.ToTable("Aggregate");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Policy)
                .HasConversion(
                    v => v.GetType().Name,
                    v => (IPolicy)Activator.CreateInstance(Type.GetType(v)));
        }
    }
}
