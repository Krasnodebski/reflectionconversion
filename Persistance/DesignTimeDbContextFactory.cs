﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace ReflectionConversion.Persistance
{
    internal class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ReflectionConversionContext>
    {
        public ReflectionConversionContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<ReflectionConversionContext> optionsBuilder = new DbContextOptionsBuilder<ReflectionConversionContext>();
            return new ReflectionConversionContext(optionsBuilder.Options);
        }
    }
}