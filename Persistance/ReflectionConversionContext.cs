﻿using Microsoft.EntityFrameworkCore;

namespace ReflectionConversion.Persistance
{
    internal class ReflectionConversionContext : DbContext
    {
        public virtual DbSet<Aggregate> Aggregates { get; set; }

        public ReflectionConversionContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        public ReflectionConversionContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=TestDB;Trusted_Connection=True;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new AggregateMapping());

            base.OnModelCreating(builder);
        }
    }
}