﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ReflectionConversion.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Aggregate",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Policy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aggregate", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Aggregate");
        }
    }
}
