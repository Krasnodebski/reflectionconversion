﻿using System;

namespace ReflectionConversion.Policy
{
    internal class ConcretePolicy : IPolicy
    {
        public void DoSomething()
        {
            Console.WriteLine("ConcretePolicy");
        }
    }
}