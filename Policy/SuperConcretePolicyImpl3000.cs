﻿using System;

namespace ReflectionConversion.Policy
{
    internal class SuperConcretePolicyImpl3000 : IPolicy
    {
        public void DoSomething()
        {
            Console.WriteLine("SuperConcretePolicyImpl3000");
        }
    }
}