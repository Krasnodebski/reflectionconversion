﻿using ReflectionConversion.Persistance;
using ReflectionConversion.Policy;
using System;

namespace ReflectionConversion
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            using (var ctx = new ReflectionConversionContext())
            {
                IPolicy policy = new ConcretePolicy();

                var id = Guid.NewGuid();
                var aggregate = new Aggregate(id, policy);

                ctx.Aggregates.Add(aggregate);
                ctx.SaveChanges();

                aggregate = ctx.Aggregates.Find(id);
                aggregate.Policy.DoSomething();

                aggregate.Policy = new SuperConcretePolicyImpl3000();
                ctx.SaveChanges();

                aggregate = ctx.Aggregates.Find(id);
                aggregate.Policy.DoSomething();
            }
        }
    }
}